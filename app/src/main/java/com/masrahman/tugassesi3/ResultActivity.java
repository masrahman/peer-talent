package com.masrahman.tugassesi3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {
    TextView tvNama;
    Button btnCange;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        tvNama = (TextView)findViewById(R.id.resultNama);
        btnCange = (Button)findViewById(R.id.resultChange);
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            tvNama.setText(bundle.getString("keynama"));
            if(bundle.getString("proses").equals("fix")){
                btnCange.setVisibility(View.GONE);
            }
        }
        btnCange.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ChangeActivity.class);
                intent.putExtra("keyname", tvNama.getText().toString());
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==1){
            if(resultCode==RESULT_OK){
                Bundle bundle = data.getExtras();
                tvNama.setText(bundle.getString("result"));
            }
        }
    }
}
