package com.masrahman.tugassesi3;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class FragmentActivity extends AppCompatActivity {
    Button btnOne, btnTwo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
        btnOne = (Button)findViewById(R.id.fragment1);
        btnTwo = (Button)findViewById(R.id.fragment2);
        getFragment(1);
        btnOne.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                getFragment(1);
            }
        });
        btnTwo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                getFragment(2);
            }
        });
    }
    void getFragment(Integer val){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if(val==1){
            FOne fOne = new FOne();
            fragmentTransaction.replace(R.id.fragmentFrame, fOne);
        }else {
            FTwo fTwo = new FTwo();
            fragmentTransaction.replace(R.id.fragmentFrame, fTwo);
        }
        fragmentTransaction.commit();
    }
}
