package com.masrahman.tugassesi3;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by USER on 18-Oct-17.
 */

public class TabAdapter extends FragmentPagerAdapter {
    FOne fOne;
    FTwo fTwo;
    public  TabAdapter(FragmentManager fm){
        super(fm);
        fOne = new FOne();
        fTwo = new FTwo();
    }
    @Override
    public Fragment getItem(int position) {
        if(position==0){
            return fOne;
        }else if(position==1){
            return  fTwo;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}