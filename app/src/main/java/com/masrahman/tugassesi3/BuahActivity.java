package com.masrahman.tugassesi3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class BuahActivity extends AppCompatActivity {
    private List<BuahModel> listBuah;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private BuahAdapter mAdapter;
    String[] buah = {"Apel", "Mangga", "Jeruk","Pepaya","Pisang","Lengkeng", "Anggur", "Rambutan", "Salak", "Melon", "Semangka", "Sirsak", "Strawberry", "Nanas", "Leci"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buah);
        recyclerView = (RecyclerView)findViewById(R.id.buahRecycle);
        listBuah = new ArrayList<>();
        mAdapter = new BuahAdapter(BuahActivity.this, listBuah);
        mLayoutManager = new LinearLayoutManager(BuahActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        addBuah();
    }
    void addBuah(){
        for(int i=0; i<buah.length;i++){
            BuahModel model = new BuahModel(buah[i]);
            listBuah.add(model);
        }
    }
}
