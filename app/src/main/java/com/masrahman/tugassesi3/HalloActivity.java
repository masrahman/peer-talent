package com.masrahman.tugassesi3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class HalloActivity extends AppCompatActivity {
    EditText etNama;
    Button btnSubmit;
    String proses;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hallo);
        Bundle bundle = getIntent().getExtras();
        proses = bundle.getString("proses");
        etNama = (EditText)findViewById(R.id.haloNama);
        btnSubmit = (Button)findViewById(R.id.haloSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ResultActivity.class);
                intent.putExtra("keynama", etNama.getText().toString());
                intent.putExtra("proses", proses);
                startActivity(intent);
            }
        });
    }
}
