package com.masrahman.tugassesi3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MenuActivity extends AppCompatActivity {
    Button task1, task2, task3, task4, task5, task6, task7;
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        task1 = (Button)findViewById(R.id.menutask1);
        task2 = (Button)findViewById(R.id.menutask2);
        task3 = (Button)findViewById(R.id.menutask3);
        task4 = (Button)findViewById(R.id.menutask4);
        task5 = (Button)findViewById(R.id.menutask5);
        task6 = (Button)findViewById(R.id.menutask6);
        task7 = (Button)findViewById(R.id.menutask7);
        task1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                intent = new Intent(getApplicationContext(), HalloActivity.class);
                intent.putExtra("proses", "fix");
                startActivity(intent);
            }
        });
        task2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                intent = new Intent(getApplicationContext(), HalloActivity.class);
                intent.putExtra("proses", "change");
                startActivity(intent);
            }
        });
        task3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                intent = new Intent(getApplicationContext(), FragmentActivity.class);
                startActivity(intent);
            }
        });
        task4.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                intent = new Intent(getApplicationContext(), HometabActivity.class);
                startActivity(intent);
            }
        });
        task5.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                intent = new Intent(getApplicationContext(), BuahActivity.class);
                startActivity(intent);
            }
        });
        task6.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                intent = new Intent(getApplicationContext(), ListbuahActivity.class);
                startActivity(intent);
            }
        });
        task7.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                intent = new Intent(getApplicationContext(), InsertdataActivity.class);
                startActivity(intent);
            }
        });
    }
}