package com.masrahman.tugassesi3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class InsertdataActivity extends AppCompatActivity {
    EditText etNama, etEmail, etTelp;
    Button btnSave, btnView;
    DatabaseHelper dbhelp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insertdata);
        etNama = (EditText)findViewById(R.id.insertNama);
        etEmail = (EditText)findViewById(R.id.insertEmail);
        etTelp = (EditText)findViewById(R.id.insertTelp);
        btnSave = (Button)findViewById(R.id.insertSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etNama.getText().toString().equals("") || etEmail.getText().toString().equals("") || etTelp.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"Isi dengan lengkap!", Toast.LENGTH_SHORT).show();
                }else{
                    dbhelp = new DatabaseHelper(getApplicationContext());
                    dbhelp.insertName(etNama.getText().toString(), etEmail.getText().toString(), etTelp.getText().toString());
                    etNama.setText("");
                    etEmail.setText("");
                    etTelp.setText("");
                    Toast.makeText(getApplicationContext(), "Data berhasil disimpan", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnView = (Button)findViewById(R.id.insertView);
        btnView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ListnamaActivity.class);
                startActivity(intent);
            }
        });
    }
}