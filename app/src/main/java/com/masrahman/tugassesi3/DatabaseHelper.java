package com.masrahman.tugassesi3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by USER on 23-Oct-17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DBNAME = "listname.sqlite";
    public static final String DBLOCATION = "/data/data/com.masrahman.tugassesi3/databases/";
    private Context cnt;
    //private SQLiteDatabase mDB;

    public DatabaseHelper(Context context) {
        super(context, DBNAME, null, 1);
        this.cnt = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE nama_nama (id INTEGER PRIMARY KEY AUTOINCREMENT,nama TEXT,email TEXT, telp TEXT)";
        sqLiteDatabase.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS nama_nama");
        onCreate(sqLiteDatabase);
    }

    /*public void openDB() {
        String dbPath = cnt.getDatabasePath(DBNAME).getPath();
        File file = new File(dbPath);
        if (file.exists() && !file.isDirectory()){
            try{
                copyDatabase();
            }catch (IOException e){
                throw new Error("Error Copy Database");
            }
        }
        mDB = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.OPEN_READWRITE);
    }*/

    /*private void copyDatabase() throws IOException {
        InputStream myInput= cnt.getAssets().open(DBNAME);
        String outFileName=DBLOCATION+DBNAME;
        OutputStream myOutput=new FileOutputStream(outFileName);
        byte[] buffer=new byte [1024];
        int length;
        while((length=myInput.read(buffer))>0){
            myOutput.write(buffer,0,length);
        }
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }*/

    /*public void closeDB(){
        if(mDB!=null && mDB.isOpen()){
            mDB.close();
        }
    }*/
    /*opertion CRUD table nama_nama
    id integer AI
    nama text
    email text
    telp numeric
    */
    public Cursor fetchAll() throws SQLException{
        return(this.getReadableDatabase().rawQuery("select * from nama_nama", null));
    }

    public void insertName(String nama, String email, String telp){
        this.getWritableDatabase().execSQL("insert into nama_nama (nama, email, telp) values ('"+ nama + "','"+ email+"','"+telp+"')");
    }

    public void deleteName(int id){
        this.getWritableDatabase().delete("nama_nama","id="+id,null);
    }

    public void updateName(int id, String nama, String email, String telp){
        this.getWritableDatabase().execSQL("update nama_nama set nama='"+nama+"', email='"+email+"', telp='"+telp+"' where id="+id);
    }
}
