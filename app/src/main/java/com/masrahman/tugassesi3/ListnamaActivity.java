package com.masrahman.tugassesi3;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class ListnamaActivity extends AppCompatActivity {
    private List<NamaModel> list;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private NamaAdapter adapter;
    DatabaseHelper dbhelp;
    Cursor mCursor;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listnama);
        recyclerView = (RecyclerView)findViewById(R.id.listnamaRecycle);
        list = new ArrayList<>();
        adapter = new NamaAdapter(list, ListnamaActivity.this);
        layoutManager = new LinearLayoutManager(ListnamaActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        bindData();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        bindData();
    }

    private void bindData(){
        list.clear();
        dbhelp = new DatabaseHelper(this);
        new AmbilData().execute();
    }

    ProgressDialog progressDialog;

    public class AmbilData extends AsyncTask<String, String, String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ListnamaActivity.this);
            progressDialog.setMessage("Loading data...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            mCursor = dbhelp.fetchAll();
            if(mCursor.getCount()<=0){

            }else{
                mCursor.moveToFirst();
                while(!mCursor.isAfterLast()){
                    NamaModel model = new NamaModel(mCursor.getInt(0), mCursor.getString(1), mCursor.getString(2), mCursor.getString(3));
                    list.add(model);
                    mCursor.moveToNext();
                }
            }
            mCursor.close();
            return null;
        }
    }

    public class NamaAdapter extends RecyclerView.Adapter<NamaAdapter.ViewHolder>{
        private List<NamaModel> list;
        private Context cnt;

        public NamaAdapter(List<NamaModel> list, Context cnt) {
            this.list = list;
            this.cnt = cnt;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            View cvView;
            TextView tvID, tvNama, tvEmail, tvTelp;
            ImageButton btnEdit, btnDel;
            public ViewHolder(View view) {
                super(view);
                cvView = (View)view.findViewById(R.id.listnamaCardview);
                tvID = (TextView)view.findViewById(R.id.listnamaID);
                tvNama = (TextView) view.findViewById(R.id.listnamaNama);
                tvEmail = (TextView) view.findViewById(R.id.listnamaEmail);
                tvTelp = (TextView) view.findViewById(R.id.listnamaPhone);
                btnEdit = (ImageButton) view.findViewById(R.id.listnamaEdit);
                btnDel = (ImageButton) view.findViewById(R.id.listnamaDel);
            }
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_nama, parent, false);
            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final NamaModel model = list.get(position);
            holder.tvID.setText(String.valueOf(model.getId()));
            holder.tvNama.setText(model.getNama());
            holder.tvEmail.setText(model.getEmail());
            holder.tvTelp.setText(model.getTelp());
            holder.btnDel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showPrompt(model.getId());
                }
            });
            holder.btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getApplicationContext(), UpdatedataActivity.class);
                    intent.putExtra("dataid", model.getId());
                    intent.putExtra("datanama", model.getNama());
                    intent.putExtra("dataemail", model.getEmail());
                    intent.putExtra("datatelp", model.getTelp());
                    startActivity(intent);
                }
            });
        }

        private void showPrompt(final int x){
            new AlertDialog.Builder(ListnamaActivity.this)
                    .setTitle(getResources().getString(R.string.confirmation))
                    .setMessage(getResources().getString(R.string.sure))
                    .setPositiveButton(getResources().getString(R.string.y),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dbhelp.deleteName(x);
                                    bindData();
                                }
                            })
                    .setNegativeButton(getResources().getString(R.string.n),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            }).show();
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
    }
}