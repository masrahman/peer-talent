package com.masrahman.tugassesi3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ChangeActivity extends AppCompatActivity {
    EditText etNama;
    Button btnChange;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change);
        etNama = (EditText)findViewById(R.id.changeNama);
        btnChange = (Button)findViewById(R.id.changeSubmit);
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            etNama.setText(bundle.getString("keyname"));
        }
        btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ResultActivity.class);
                intent.putExtra("result", etNama.getText().toString());
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
