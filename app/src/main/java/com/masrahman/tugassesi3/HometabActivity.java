package com.masrahman.tugassesi3;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

public class HometabActivity extends AppCompatActivity {
    ViewPager viewPager;
    TabLayout tabLayout;
    FOne fOne;
    FTwo fTwo;
    TabLayout.Tab content_1, content_2;
    TabAdapter tabAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hometab);
        viewPager = (ViewPager)findViewById(R.id.hometabPager);
        tabLayout = (TabLayout)findViewById(R.id.homeTab);
        tabAdapter = new TabAdapter(getSupportFragmentManager());
        viewPager.setAdapter(tabAdapter);
        tabLayout.setupWithViewPager(viewPager);
        fOne = (FOne)tabAdapter.getItem(0);
        fTwo = (FTwo)tabAdapter.getItem(1);
        content_1 = tabLayout.getTabAt(0);
        content_2 = tabLayout.getTabAt(1);
        viewPager.setOffscreenPageLimit(2);

        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.tab_item, null);
        tabOne.setText("Content 1");
        content_1.setCustomView(tabOne);
        tabOne.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.tab_item, null);
        tabTwo.setText("Content 2");
        content_2.setCustomView(tabTwo);
        tabTwo.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
    }
}
