package com.masrahman.tugassesi3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class UpdatedataActivity extends AppCompatActivity {
    EditText etNama, etEmail, etTelp;
    Button btnSave;
    DatabaseHelper dbhelp;
    Integer idx = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_updatedata);
        etNama = (EditText)findViewById(R.id.updateNama);
        etEmail = (EditText)findViewById(R.id.updateEmail);
        etTelp = (EditText)findViewById(R.id.updateTelp);
        btnSave = (Button)findViewById(R.id.updateSave);
        dbhelp = new DatabaseHelper(this);
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            idx = bundle.getInt("dataid");
            etNama.setText(bundle.getString("datanama"));
            etEmail.setText(bundle.getString("dataemail"));
            etTelp.setText(bundle.getString("datatelp"));
        }
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(idx==null){
                    Toast.makeText(getApplicationContext(), "Tidak ada ID untuk dilakukan Perubahan", Toast.LENGTH_SHORT).show();
                }else{
                    dbhelp.updateName(idx, etNama.getText().toString(), etEmail.getText().toString(), etTelp.getText().toString());
                    finish();
                }
            }
        });
    }
}