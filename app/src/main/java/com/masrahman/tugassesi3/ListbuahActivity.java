package com.masrahman.tugassesi3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ListbuahActivity extends AppCompatActivity {
    private List<BuahModel> listBuah;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutmanager;
    private BuahAdapter mAdapter;
    String[] angka = {"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listbuah);
        recyclerView = (RecyclerView)findViewById(R.id.listbuahRecycle);
        listBuah = new ArrayList<>();
        mAdapter = new BuahAdapter(ListbuahActivity.this, listBuah);
        mLayoutmanager = new GridLayoutManager(ListbuahActivity.this, 3);
        recyclerView.setLayoutManager(mLayoutmanager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        getData();
    }

    void getData(){
        for(int i=0; i<angka.length; i++){
            BuahModel model = new BuahModel(angka[i]);
            listBuah.add(model);
        }
    }
}
