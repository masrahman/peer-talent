package com.masrahman.tugassesi3;

/**
 * Created by USER on 23-Oct-17.
 */

public class NamaModel {
    private int id;
    private String nama;
    private String email;
    private String telp;

    public NamaModel(int id, String nama, String email, String telp) {
        this.id = id;
        this.nama = nama;
        this.email = email;
        this.telp = telp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }
}
