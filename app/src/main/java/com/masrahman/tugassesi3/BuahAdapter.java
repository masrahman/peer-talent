package com.masrahman.tugassesi3;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by USER on 15-Oct-17.
 */

public class BuahAdapter extends RecyclerView.Adapter<BuahAdapter.ViewHolder> {
    private List<BuahModel> list;
    private Context cnt;
    public  BuahAdapter(Context context, List<BuahModel> lst){
        this.cnt = context;
        this.list = lst;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvNamabuah;
        public View cardView;
        public ViewHolder(View view){
            super(view);
            cardView = (View)view.findViewById(R.id.listbuahNama);
            tvNamabuah = (TextView)view.findViewById(R.id.listbuahNama);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_buah, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final BuahModel model = list.get(position);
        holder.tvNamabuah.setText(model.getNamabuah());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(cnt, holder.tvNamabuah.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}